# Spark 审核后台

Spark审核后台是一个基于Laravel框架并集成Laravel-admin组件的web后台。主要功能是审核（新用户、被举报用户、修改用户资料的用户）用户的Profile信息或者举报信息，然后做出强制修改、封禁或者白名单处理。

#### 一、审核后台相关目录文件介绍：
```
├──app
│ └─Admin (后台管理)
│   ├── Controllers
│   │ ├── OperationReviewController.php（实习生审核相关逻辑）
│   │ ├── OperationReviewOnlineController.php（管理员审核相关逻辑）
│   │ ├── ReviewImageNotificationController.php（官方图片发送）
│   │ ├── ReviewNotificationController.php（官方文字消息发送）
│   │ └── UserFeedBackController.php（用户反馈）
│   ├── Models
│   │ └── ReviewNotification.php（官方通知发送历史记录）
│   └── routes.php（后台url路由）
│ └─Api (APP后端接口：核心业务逻辑)
│   ├── Models
│   │ ├── Feedback（用户反馈模型）
│   │ └── OperationReview（审核相关模型）
│   ├── Traits
│   │ └── OperationReviewManager.php（审核相关公用函数）
│   └── Versions (Api版本控制：核心controller)
│       ├──v1 
│       │  └──Controllers
│       │      └── ReportController.php（用户被举报后推审核）
│       └──v3
│          └──Controllers
│              └── UserProfileController.php（新用户和二次编辑Profile后推审核）
├──config
│ └─admin.php (后台配置文件)
└──resources
    ├── js
    │ └── admin.js（后台js文件 ）
    ├── sass
    │ └── admin.scss（后台scss文件）
    └─ views
      └──admin
         └──custom_blade（自定义视图模板）
            └──review
              ├──admin_review_profile.blade.php（管理员审核列表页面）
              ├──review_profile.blade.php（实习生审核列表页面）
              └──review_profile_edit.blade.php（管理员和实习生审核详情页）

```
#### 二、审核后台相关流程：
##### 1.新用户：
```
ProfileUpdate ——> 新建review ——> 人工审核 ——> 上线
            └──> 图片检测 ——> 通过 ——> 人工审核 ——> 上线
                        └──> 不通过（图片封禁） ——> 上线（强制修改）
```
##### 2.二次编辑：
```
ProfileUpdate ——> 标注项是否全部修改 ——> 是（黑名单解禁、解除强制修改）  ——>  人工审核 ——> 上线
                             └──> 否 ——>  不做处理
```
```
PhoteDelete  ——> 标注项是否全部修改 ——>   是（解除强制修改) 
```
```  
PhotoUpdate ——>标注项是否全部修改——> 是（解除强制修改)  ——> 图片检测 ——> 通过（图片解禁） ——> 标注项是否全部修改 ——> 是（黑名单解、解除强制修改）  ——> 人工审核 ——> 上线
                                                            └──> 不通过（图片封禁） ——> 上线（强制修改）                       
```
##### 3.被普通用户举报：
```
Report  ——>  人工审核 ——> 上线
```
##### 4.被超级VIP举报：
```
Report  ——>  上线（封禁3天、黑名单封禁）
    └──> 人工审核 ——> 上线 
```
##### 5.管理员搜索用户审核：
```
Admin Review ——> 管理员审核 ——> 上线
```
